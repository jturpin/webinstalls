#!/bin/bash
if [ "$#" -ne 3 ]; then
  echo "Usage: $0 <KAFINEA_OFFLINE_LOGIN> <KAFINEA_OFFLINE_PASSWORD> <KAFINEA_BIRT_PASSWORD>"
  exit
fi

export KAFINA_OFFLINE_USER=$1
export KAFINA_OFFLINE_PASSWORD=$2
export KAFINA_BIRT_PASSWORD=$3

cd /usr/local
if [ -d /usr/local/kafinea ]; then
        echo "kafinea installation already exists. Please backup first. Aborting."
        exit
fi
sudo git clone "https://${1}:${2}@gitlab.com/madiasoft/kafinea-offline.git"
sudo mv kafinea-offline kafinea && cd kafinea
#Initial install (we remove locks and flag with the appropriate instruction)
sudo rm -f /tmp/lock.txt
sudo echo "UPDATE" > /usr/local/kafinea/conf/updatekafinea.txt
sudo ./bin/install_dependencies.sh
sudo ./bin/download_latest_kafinea.sh $KAFINA_OFFLINE_USER $KAFINA_OFFLINE_PASSWORD $KAFINA_BIRT_PASSWORD
sudo start-kafinea
